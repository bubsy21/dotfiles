-- [[ Configure and install plugins ]]
--
--  To check the current status of your plugins, run
--    :Lazy
--
--  You can press `?` in this menu for help. Use `:q` to close the window
--
--  To update plugins you can run
--    :Lazy update
--
-- NOTE: Here is where you install your plugins.
require('lazy').setup({
  -- NOTE: Plugins can be added with a link (or for a github repo: 'owner/repo' link).
  'tpope/vim-sleuth', -- Detect tabstop and shiftwidth automatically

  -- NOTE: Plugins can also be added by using a table,
  -- with the first argument being the link and the following
  -- keys can be used to configure plugin behavior/loading/etc.
  --
  -- Use `opts = {}` to force a plugin to be loaded.
  --
  --  This is equivalent to:
  --    require('Comment').setup({})

  -- "gc" to comment visual regions/lines
  { 'numToStr/Comment.nvim', opts = {} },

  -- modular plugin config below
  -- /lua/pathname/*.lua
  --
  -------------------
  --kickstart.nvim--
  -------------------
  --
  -- fast git integration for nvim buffers --
  require 'kickstart.plugins.gitsigns',

  -- a display for the possible keybinds of cmd you started typing --
  require 'kickstart.plugins.which-key',

  -- highly extendable fuzzy finder :D --
  require 'kickstart.plugins.telescope',

  -- configs for the nvim lsp client --
  require 'kickstart.plugins.lspconfig',

  -- lightweight yet powerful formatter plugin for neovim --
  require 'kickstart.plugins.conform',

  -- a code completion engine for nvim --
  require 'kickstart.plugins.cmp',

  -- a clean dark nvim theme reminiscent of catpuccin --
  require 'kickstart.plugins.tokyonight',

  -- highlight  and search for todo comments in different styles --
  require 'kickstart.plugins.todo-comments',

  -- library of lua modules for nvim; a swiss army knife plugin --
  require 'kickstart.plugins.mini',

  -- a parsing system with some basic funcionality such as highlighting --
  require 'kickstart.plugins.treesitter',

  -- brings autopairs to nvim; modifies behavior of certain keystrokes to help keep paired characters properly matched--
  require 'kickstart.plugins.autopairs',


  --
  -----------------------
  -- my custom plugins --
  -----------------------
  --
  -- small plugin to make working w/ ansible more convenient --
  -- require 'custom.plugins.ansible-vim',

  -- a nice tabline plugin --
  require 'custom.plugins.barbar',

  -- discord rich presence plugin to flex i use neovim btw --
  require 'custom.plugins.cord',

  -- fast nvim statusline --
  require 'custom.plugins.lualine',

  -- vim like file explorer that lets you edit your filesystem like a normal nvim buffer --
  require 'custom.plugins.oil',

  -- a prettly list for showing diagnostic, references, telescope results, quickfix and location lists to help you solve problems --
  require 'custom.plugins.trouble', --

  -- vim game by ThePrmeagen to get better at vim movements --
  require 'custom.plugins.vim-be-good',

  -- navigate code with search labels,enhanced char motions, and treesitter integration
  require 'custom.plugins.flash',

  -- getting you where you want with the fewest keystrokes
  require 'custom.plugins.harpoon',

  -- calls lazygit from within neovim; a simple terminal UI for git commands
  require 'custom.plugins.lazygit',

  ------------
  -- themes --
  ------------
  require 'custom.plugins.rose-pine',

  --
  -- this is a nice line to simplify including all custom plugins... but we like modularity and being able to pinpoint when something goes wrong with pcall'ing
  -- { import = 'custom.plugins' },
}, {
  ui = {
    -- If you are using a Nerd Font: set icons to an empty table which will use the
    -- default lazy.nvim defined Nerd Font icons, otherwise define a unicode icons table
    icons = vim.g.have_nerd_font and {} or {
      cmd = '⌘',
      config = '🛠',
      event = '📅',
      ft = '📂',
      init = '⚙',
      keys = '🗝',
      plugin = '🔌',
      runtime = '💻',
      require = '🌙',
      source = '📄',
      start = '🚀',
      task = '📌',
      lazy = '💤 ',
    },
  },
})

-- vim: ts=2 sts=2 et
