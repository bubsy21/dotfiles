#!/bin/bash

# set default audio device
pactl set-default-sink alsa_output.pci-0000_08_00.1.hdmi-stereo

uncomment_line() {
	local file="$1"
	local pattern="$2"
	sed -i "s/^#\(${pattern}\)/\1/" "$file"
}

comment_line() {
	local file="$1"
	local pattern="$2"
	sed -i "s/^\(${pattern}\)/#\1/" "$file"
}


###
# update userprefs.conf 
# NOT FUNCTIONAL - it uncomments and comments the steam line fine; but fails to do the same for others.
# big assumptions here - but i think it is likely to do with the '[]' inside the other lines causing it to fail :shrug:
###
#comment_line ~/.config/hypr/userprefs.conf "exec-once=[workspace 1 silent] floorp"
#comment_line ~/.config/hypr/userprefs.conf "exec-once=[workspace 2 silent] obsidian"
#comment_line ~/.config/hypr/userprefs.conf "exec-once=[workspace 2 silent] vesktop"
#uncomment_line ~/.config/hypr/userprefs.conf "exec-once= steam"


# update userprefs.conf
sed -i '/#work mode/,/#game mode/ {/exec-once=/s/^#*//; /exec-once=/s/^/#/}' ~/.config/hypr/userprefs.conf
sed -i '/#game mode/,/# #################/ {/exec-once=/s/^#*//}' ~/.config/hypr/userprefs.conf


# update monitors.conf
comment_line ~/.config/hypr/monitors.conf "monitor=DP-1,3840x1600@143.99800,0x0,1,vrr,1"
uncomment_line ~/.config/hypr/monitors.conf "monitor=HDMI-A-1,4096x2160@119.88,0x0,2,vrr,1"
comment_line ~/.config/hypr/monitors.conf "monitor=HDMI-A-1,disable"
uncomment_line ~/.config/hypr/monitors.conf "monitor=DP-1,disable"

