#!/bin/bash

## this script is solely to get around the bug with SBBM right now...
## once this is fixed - this script is no longer needed

#!/bin/bash

# kill steam
#echo "killing steam processes..."
#pkill -9 steam

# Wait for Steam to completely terminate
#echo "waiting for steam processes to terminate..."
#while pgrep -x steam >/dev/null; do
#  sleep 1
#done

# reboot steam in big picture mode
echo "launching big picture mode.."
#steam -bigpicture &
DISPLAY=:0 steam -bigpicture &
