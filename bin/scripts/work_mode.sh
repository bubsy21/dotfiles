#!/bin/bash

# set default audio device
pactl set-default-sink alsa_output.usb-Schiit_Audio_Schiit_Modi_-00.analog-stereo

uncomment_line() {
	local file="$1"
	local pattern="$2"
	sed -i "s/^#\(${pattern}\)/\1/" "$file"
}

comment_line() {
	local file="$1"
	local pattern="$2"
	sed -i "s/^\(${pattern}\)/#\1/" "$file"
}


# Debugging: Print the file before changes
#echo "Before changes to userprefs.conf:"
#cat ~/.config/hypr/userprefs.conf

# below not functional - see gaming mode for more info
#uncomment_line ~/.config/hypr/userprefs.conf "exec-once=[workspace 1 silent] floorp"
#uncomment_line ~/.config/hypr/userprefs.conf "exec-once=[workspace 2 silent] obsidian"
#uncomment_line ~/.config/hypr/userprefs.conf "exec-once=[workspace 2 silent] vesktop"
#comment_line ~/.config/hypr/userprefs.conf "exec-once= steam"

# Debugging: Print the file after changes
#echo "After changes to userprefs.conf:"
#cat ~/.config/hypr/userprefs.conf


# update userprefs.conf
sed -i '/#game mode/,/# #################/ {/exec-once=/s/^#*//; /exec-once=/s/^/#/}' ~/.config/hypr/userprefs.conf
sed -i '/#work mode/,/#game mode/ {/exec-once=/s/^#*//}' ~/.config/hypr/userprefs.conf


# update monitors.conf
uncomment_line ~/.config/hypr/monitors.conf "monitor=DP-1,3840x1600@143.99800,0x0,1,vrr,1"
comment_line ~/.config/hypr/monitors.conf "monitor=HDMI-A-1,4096x2160@119.88,0x0,2,vrr,1"
uncomment_line ~/.config/hypr/monitors.conf "monitor=HDMI-A-1,disable"
comment_line ~/.config/hypr/monitors.conf "monitor=DP-1,disable"
